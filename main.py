# -*- coding: utf-8 -*-

# File Name: main.py
# Author: 开源吧
# Email: 2219126560@qq.com
# URL: http://120.27.245.185/
# Date: 2024-05-05
# Version: 1.0
# Description: 用于多开微信的一个窗口应用

import os
import tkinter as tk
import subprocess
import pickle

data_file = 'cache.pkl'
data = {
    'url': None,
    'num': None
}

def cache_data(data_file, data):
    with open(data_file, 'wb') as file:
        pickle.dump(data, file)

def load_data(data_file):
    if os.path.exists(data_file):
        with open(data_file, 'rb') as file:
            return pickle.load(file)
    return None

def run(url, num):
    url = url.replace('\n', '')
    for i in range(int(num)):
        subprocess.Popen(url)
    data['url'] = url
    data['num'] = num
    cache_data(data_file, data)

def main():
    window = tk.Tk()
    window.geometry('500x80+500+350')
    window.title("微信多开 (开源吧:2219126560@qq.com)")

    url_label = tk.Label(window, text="微信地址:", )
    url_label.grid(row=0, column=0)
    url_text = tk.Text(window, width=42, height=1)
    url_text.grid(row=0, column=1)

    quantity_label = tk.Label(window, text="启动数量:", )
    quantity_label.grid(row=1, column=0)
    quantity_text = tk.Text(window, width=42, height=1)
    quantity_text.grid(row=1, column=1)

    cached_data = load_data(data_file)
    if cached_data:
        url = cached_data['url']
        num = cached_data['num']
        url_text.insert('insert', url)
        quantity_text.insert('insert', num)


    button = tk.Button(window, height=1, width=8, text="运行")
    button.config(command=lambda: run(url_text.get('1.0', tk.END), quantity_text.get('1.0', tk.END)))
    button.grid(row=2, column=0)

    tk.mainloop()

if __name__ == "__main__":
    main()
